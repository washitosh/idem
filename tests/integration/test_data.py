import os

from tests.runner import run_sls


def test_data_write(hub, code_dir):
    try:
        ret = run_sls(["data"])
        assert len(ret) == 4, "Expecting 4 results"
        result = ret.get(
            "data_|-test-output-empty-file-name_|-test-output-empty-file-name_|-write"
        )
        assert result["result"] is False
        assert "not found" in str(result["comment"]), result["comment"]

        result = ret.get(
            "data_|-test-output-missing-template_|-test-output-missing-template_|-write"
        )
        assert result["result"] is False
        assert "not found" in str(result["comment"]), result["comment"]

        result = ret.get(
            "data_|-test-output-no-template_|-test-output-no-template_|-write"
        )
        assert result["result"] is True
        assert result["new_state"]

        result = ret.get(
            "data_|-test-output-with-template_|-test-output-with-template_|-write"
        )
        assert result["result"] is True
        assert result["new_state"]
    finally:
        os.remove("result.json")
