from tests.runner import run_sls_source

ACCT_DATA = {
    "profiles": {
        "my_provider": {"default": {"kw1": "val1"}, "my_profile": {"kw1": "val1"}}
    }
}


def test_default(hub):
    ret = run_sls_source(
        sls=["location"],
        sls_sources=[f"my_provider://source_location"],
        acct_data=ACCT_DATA,
    )
    assert ret["test_|-location_|-location_|-present"]["new_state"] == {
        "ctx": {"kw1": "val1"},
        "protocol": "my_provider",
        "source": "source_location",
    }


def test_profile(hub):
    ret = run_sls_source(
        sls=["location"],
        sls_sources=[f"my_provider://my_profile@source_location"],
        acct_data=ACCT_DATA,
    )

    assert ret["test_|-location_|-location_|-present"]["new_state"] == {
        "ctx": {"kw1": "val1"},
        "protocol": "my_provider",
        "source": "source_location",
    }
