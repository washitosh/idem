def test_catching_errors(hub):
    ret = hub.exec.tests.fail.error()
    assert (
        ret.comment
        == "ValueError: This gets transformed into an ExecReturn with a failure"
    )
    assert ret.result is False
    assert not ret
    assert ret.ret is None
