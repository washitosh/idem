import pytest

from tests.runner import tpath_hub


@pytest.fixture(scope="function", name="hub")
def contracts_tpath_hub():
    with tpath_hub() as hub:
        yield hub
