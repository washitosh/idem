import ast
import json
import subprocess
import sys
import tempfile


def test_cli(runpy):
    # Run the describe command
    cmd = [sys.executable, runpy, "describe", "test", "--output=json"]
    ret = subprocess.run(cmd, capture_output=True, check=True, encoding="utf-8", env={})
    assert ret.returncode == 0, ret.stderr
    yaml = ret.stdout

    fh = tempfile.NamedTemporaryFile("w+", suffix=".sls")
    fh.write(yaml)
    fh.flush()

    # Verify that passing states were created from it
    cmd = [sys.executable, runpy, "state", fh.name, "--output=json", "--runtime=serial"]
    ret = subprocess.run(cmd, capture_output=True, check=True, encoding="utf-8", env={})
    fh.close()
    assert ret.stdout
    data = json.loads(ret.stdout)

    assert ret.returncode == 0, ret.stderr
    assert data == {
        "test_|-Description of test.anop_|-anop_|-anop": {
            "name": "anop",
            "changes": {},
            "result": True,
            "comment": "Success!",
            "__run_num": 1,
        },
        "test_|-Description of test.configurable_test_state_|-configurable_test_state_|-configurable_test_state": {
            "name": "configurable_test_state",
            "changes": {
                "testing": {"old": "Unchanged", "new": "Something pretended to change"}
            },
            "result": True,
            "comment": "",
            "__run_num": 2,
        },
        "test_|-Description of test.describe_|-Description of test.describe_|-describe": {
            "Description of test.anop": {"test.anop": [{"name": "anop"}]},
            "Description of test.configurable_test_state": {
                "test.configurable_test_state": [
                    {"name": "configurable_test_state"},
                    {"changes": True},
                    {"result": True},
                    {"comment": ""},
                ]
            },
            "Description of test.describe": {"test.describe": []},
            "Description of test.fail_with_changes": {
                "test.fail_with_changes": [{"name": "fail_with_changes"}]
            },
            "Description of test.fail_without_changes": {
                "test.fail_without_changes": [{"name": "fail_without_changes"}]
            },
            "Description of test.mod_watch": {
                "test.mod_watch": [{"name": "mod_watch"}]
            },
            "Description of test.none_without_changes": {
                "test.none_without_changes": [{"name": "none_without_changes"}]
            },
            "Description of test.nop": {"test.nop": [{"name": "nop"}]},
            "Description of test.succeed_with_changes": {
                "test.succeed_with_changes": [{"name": "succeed_with_changes"}]
            },
            "Description of test.succeed_with_comment": {
                "test.succeed_with_comment": [
                    {"name": "succeed_with_comment"},
                    {"comment": None},
                ]
            },
            "Description of test.succeed_without_changes": {
                "test.succeed_without_changes": [{"name": "succeed_without_changes"}]
            },
            "Description of test.treq": {"test.treq": [{"name": "treq"}]},
            "Description of test.unique_op": {
                "test.unique_op": [{"name": "unique_op"}]
            },
            "Description of test.update_low": {
                "test.update_low": [{"name": "update_low"}]
            },
            "__run_num": 3,
        },
        "test_|-Description of test.fail_with_changes_|-fail_with_changes_|-fail_with_changes": {
            "name": "fail_with_changes",
            "changes": {
                "testing": {"old": "Unchanged", "new": "Something pretended to change"}
            },
            "result": False,
            "comment": "Failure!",
            "__run_num": 4,
        },
        "test_|-Description of test.fail_without_changes_|-fail_without_changes_|-fail_without_changes": {
            "name": "fail_without_changes",
            "changes": {},
            "result": False,
            "comment": "Failure!",
            "__run_num": 5,
        },
        "test_|-Description of test.mod_watch_|-mod_watch_|-mod_watch": {
            "name": "mod_watch",
            "changes": {"watch": True},
            "result": True,
            "comment": "Watch ran!",
            "__run_num": 6,
        },
        "test_|-Description of test.none_without_changes_|-none_without_changes_|-none_without_changes": {
            "name": "none_without_changes",
            "changes": {},
            "result": None,
            "comment": "Success!",
            "__run_num": 7,
        },
        "test_|-Description of test.nop_|-nop_|-nop": {
            "name": "nop",
            "changes": {},
            "result": True,
            "comment": "Success!",
            "__run_num": 8,
        },
        "test_|-Description of test.succeed_with_changes_|-succeed_with_changes_|-succeed_with_changes": {
            "name": "succeed_with_changes",
            "old_state": {},
            "new_state": {
                "testing": {"old": "Unchanged", "new": "Something pretended to change"},
                "tests": [[{"new": "new_test"}]],
            },
            "changes": {
                "testing": {"old": "Unchanged", "new": "Something pretended to change"},
                "tests": [[{"new": "new_test"}]],
            },
            "result": True,
            "comment": "Success!",
            "__run_num": 9,
        },
        "test_|-Description of test.succeed_with_comment_|-succeed_with_comment_|-succeed_with_comment": {
            "name": "succeed_with_comment",
            "changes": {},
            "result": True,
            "comment": None,
            "__run_num": 10,
        },
        "test_|-Description of test.succeed_without_changes_|-succeed_without_changes_|-succeed_without_changes": {
            "name": "succeed_without_changes",
            "changes": {},
            "result": True,
            "comment": "Success!",
            "__run_num": 11,
        },
        "test_|-Description of test.unique_op_|-unique_op_|-unique_op": {
            "name": "unique_op",
            "changes": {},
            "result": True,
            "comment": "Success!",
            "__run_num": 12,
        },
        "test_|-Description of test.update_low_|-update_low_|-update_low": {
            "name": "update_low",
            "changes": {},
            "result": True,
            "comment": "Success!",
            "__run_num": 13,
        },
        "test_|-king_arthur_|-totally_extra_alls_|-nop": {
            "name": "totally_extra_alls",
            "changes": {},
            "result": True,
            "comment": "Success!",
            "__run_num": 14,
        },
        "test_|-Description of test.treq_|-treq_|-treq": {
            "name": "treq",
            "changes": {},
            "result": True,
            "comment": "Success!",
            "__run_num": 15,
        },
    }


def test_jmespath_output(runpy):
    cmd = [sys.executable, runpy, "describe", "test", "--output=jmespath"]

    ret = subprocess.run(cmd, capture_output=True, check=True, encoding="utf-8", env={})
    assert ret.returncode == 0, ret.stderr
    data = ret.stdout

    assert ast.literal_eval(data) == [
        {
            "name": "Description of test.anop",
            "ref": "test.anop",
            "resource": [{"name": "anop"}],
        },
        {
            "name": "Description of test.configurable_test_state",
            "ref": "test.configurable_test_state",
            "resource": [
                {"name": "configurable_test_state"},
                {"changes": True},
                {"result": True},
                {"comment": ""},
            ],
        },
        {
            "name": "Description of test.describe",
            "ref": "test.describe",
            "resource": [],
        },
        {
            "name": "Description of test.fail_with_changes",
            "ref": "test.fail_with_changes",
            "resource": [{"name": "fail_with_changes"}],
        },
        {
            "name": "Description of test.fail_without_changes",
            "ref": "test.fail_without_changes",
            "resource": [{"name": "fail_without_changes"}],
        },
        {
            "name": "Description of test.mod_watch",
            "ref": "test.mod_watch",
            "resource": [{"name": "mod_watch"}],
        },
        {
            "name": "Description of test.none_without_changes",
            "ref": "test.none_without_changes",
            "resource": [{"name": "none_without_changes"}],
        },
        {
            "name": "Description of test.nop",
            "ref": "test.nop",
            "resource": [{"name": "nop"}],
        },
        {
            "name": "Description of test.succeed_with_changes",
            "ref": "test.succeed_with_changes",
            "resource": [{"name": "succeed_with_changes"}],
        },
        {
            "name": "Description of test.succeed_with_comment",
            "ref": "test.succeed_with_comment",
            "resource": [{"name": "succeed_with_comment"}, {"comment": None}],
        },
        {
            "name": "Description of test.succeed_without_changes",
            "ref": "test.succeed_without_changes",
            "resource": [{"name": "succeed_without_changes"}],
        },
        {
            "name": "Description of test.treq",
            "ref": "test.treq",
            "resource": [{"name": "treq"}],
        },
        {
            "name": "Description of test.unique_op",
            "ref": "test.unique_op",
            "resource": [{"name": "unique_op"}],
        },
        {
            "name": "Description of test.update_low",
            "ref": "test.update_low",
            "resource": [{"name": "update_low"}],
        },
    ]


def test_jmespath_filter(runpy):
    cmd = [
        sys.executable,
        runpy,
        "describe",
        "test",
        "--output=jmespath",
        "--filter",
        "@",
    ]

    ret = subprocess.run(cmd, capture_output=True, check=True, encoding="utf-8", env={})
    assert ret.returncode == 0, ret.stderr
    data = ret.stdout

    assert ast.literal_eval(data) == [
        {
            "name": "Description of test.anop",
            "ref": "test.anop",
            "resource": [{"name": "anop"}],
        },
        {
            "name": "Description of test.configurable_test_state",
            "ref": "test.configurable_test_state",
            "resource": [
                {"name": "configurable_test_state"},
                {"changes": True},
                {"result": True},
                {"comment": ""},
            ],
        },
        {
            "name": "Description of test.describe",
            "ref": "test.describe",
            "resource": [],
        },
        {
            "name": "Description of test.fail_with_changes",
            "ref": "test.fail_with_changes",
            "resource": [{"name": "fail_with_changes"}],
        },
        {
            "name": "Description of test.fail_without_changes",
            "ref": "test.fail_without_changes",
            "resource": [{"name": "fail_without_changes"}],
        },
        {
            "name": "Description of test.mod_watch",
            "ref": "test.mod_watch",
            "resource": [{"name": "mod_watch"}],
        },
        {
            "name": "Description of test.none_without_changes",
            "ref": "test.none_without_changes",
            "resource": [{"name": "none_without_changes"}],
        },
        {
            "name": "Description of test.nop",
            "ref": "test.nop",
            "resource": [{"name": "nop"}],
        },
        {
            "name": "Description of test.succeed_with_changes",
            "ref": "test.succeed_with_changes",
            "resource": [{"name": "succeed_with_changes"}],
        },
        {
            "name": "Description of test.succeed_with_comment",
            "ref": "test.succeed_with_comment",
            "resource": [{"name": "succeed_with_comment"}, {"comment": None}],
        },
        {
            "name": "Description of test.succeed_without_changes",
            "ref": "test.succeed_without_changes",
            "resource": [{"name": "succeed_without_changes"}],
        },
        {
            "name": "Description of test.treq",
            "ref": "test.treq",
            "resource": [{"name": "treq"}],
        },
        {
            "name": "Description of test.unique_op",
            "ref": "test.unique_op",
            "resource": [{"name": "unique_op"}],
        },
        {
            "name": "Description of test.update_low",
            "ref": "test.update_low",
            "resource": [{"name": "update_low"}],
        },
    ]


def test_filter(runpy):
    cmd = [
        sys.executable,
        runpy,
        "describe",
        "test",
        "--output=jmespath",
        "--filter",
        "[?resource[?name=='succeed_with_comment']]",
    ]

    ret = subprocess.run(cmd, capture_output=True, check=True, encoding="utf-8", env={})
    assert ret.returncode == 0, ret.stderr
    data = ret.stdout

    assert ast.literal_eval(data) == [
        {
            "name": "Description of test.succeed_with_comment",
            "resource": [{"name": "succeed_with_comment"}, {"comment": None}],
            "ref": "test.succeed_with_comment",
        },
    ], data
