import json
import subprocess
import sys

import pytest

from tests.runner import IdemRunException
from tests.runner import run_sls
from tests.runner import TESTS_DIR


def test_params(hub):
    ret = run_sls(["success"], params=["params"], sls_offset="sls/params")
    assert ret
    assert isinstance(ret, dict), ret
    assert len(ret) == 1, "Expecting 1 state"
    for state, ret in ret.items():
        assert ret["result"] is True, state
        assert ret["name"] == "Some State"
        assert ret["changes"]
        assert ret["changes"]["parameters"]
        assert ret["changes"]["parameters"]["location"] == "eastus"
        assert ret["changes"]["parameters"]["backup_location"] == "westus"
        assert ret["changes"]["parameters"]["empty"] is None
        assert ret["changes"]["parameters"]["empty_str"] == "_str"
        assert ret["changes"]["parameters"]["include_1"] == "include_1"
        assert ret["changes"]["parameters"]["include_2"] == "include_2"


def test_params_failure(hub):
    with pytest.raises(IdemRunException) as e:
        ret = run_sls(["failure"], params="params", sls_offset="sls/params")
        assert (
            "Error rendering sls: RenderException: Jinja variable 'dict object' has no attribute 'invalid'"
            == str(e)
        )


def test_params_cli(hub, runpy):
    sls_dir = TESTS_DIR / "sls"
    cmd = [
        sys.executable,
        runpy,
        "state",
        f"{sls_dir / 'params' / 'success.sls'}",
        "--param-sources",
        f"file://{sls_dir / 'params' /'params.sls' }",
        "--params",
        f"{sls_dir / 'params' / 'params.sls'}",
        "--output=json",
    ]
    ret = subprocess.run(cmd, capture_output=True, encoding="utf-8", env={})
    assert ret.returncode == 0, ret.stderr
    assert json.loads(ret.stdout) == {
        "nest.test_|-State Some State Present_|-Some State_|-succeed_with_kwargs_as_changes": {
            "__run_num": 1,
            "changes": {},
            "comment": "Could not find function to enforce nest.test.Please make sure that the corresponding plugin is loaded.",
            "name": "Some State",
            "result": False,
        }
    }


def test_params_cli_implicit(hub, runpy):
    sls_dir = TESTS_DIR / "sls"
    cmd = [
        sys.executable,
        runpy,
        "state",
        f"{sls_dir / 'params' / 'success.sls'}",
        "--param-sources",
        f"file://{sls_dir / 'params' }",
        "--params",
        "params",
        "--output=json",
    ]
    ret = subprocess.run(cmd, capture_output=True, encoding="utf-8", env={})
    assert ret.returncode == 0, ret.stderr
    assert json.loads(ret.stdout) == {
        "nest.test_|-State Some State Present_|-Some State_|-succeed_with_kwargs_as_changes": {
            "__run_num": 1,
            "changes": {},
            "comment": "Could not find function to enforce nest.test.Please make sure that the corresponding plugin is loaded.",
            "name": "Some State",
            "result": False,
        }
    }
