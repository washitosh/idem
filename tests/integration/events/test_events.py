import json
import pathlib
import subprocess
import sys
import tempfile

import pytest


@pytest.mark.asyncio
@pytest.mark.parametrize("event_acct_file", ["idem-exec", "idem-*"], indirect=True)
async def test_exec(hub, runpy, kafka, rabbitmq, event_acct_file):
    proc = subprocess.Popen(
        [
            sys.executable,
            runpy,
            "exec",
            "test.ping",
            f"--acct-file={event_acct_file}",
        ],
    )
    assert proc.wait() == 0

    expected = {
        "message": {"result": True, "ret": True},
        "tags": {"ref": "exec.test.ping", "type": "exec-post"},
        "run_name": "cli",
    }

    # Test rabbitmq
    received_message = await rabbitmq.get()
    assert json.loads(received_message.body) == expected

    # Test Kafka
    received_message = await kafka.getone()
    assert json.loads(received_message.value) == expected


@pytest.mark.asyncio
@pytest.mark.parametrize("event_acct_file", ["idem-low"], indirect=True)
async def test_low(hub, runpy, kafka, rabbitmq, event_acct_file):
    with tempfile.NamedTemporaryFile(mode="w", suffix=".sls") as fh:
        fh.write("s:\n  test.succeed_without_changes")
        fh.flush()

        proc = subprocess.Popen(
            [
                sys.executable,
                runpy,
                "state",
                fh.name,
                f"--acct-file={event_acct_file}",
                "--esm-plugin=null",
            ],
        )
        assert proc.wait() == 0

    path = pathlib.Path(fh.name)

    expected = {
        "message": [
            {
                "__id__": "s",
                "__sls__": path.stem,
                "fun": "succeed_without_changes",
                "name": "s",
                "order": 100000,
                "state": "test",
            }
        ],
        "tags": {"ref": "idem.run.init.start", "type": "state-low-data"},
        "run_name": "cli",
    }

    # Test rabbitmq
    received_message = await rabbitmq.get()
    assert json.loads(received_message.body) == expected

    # Test Kafka
    received_message = await kafka.getone()
    assert json.loads(received_message.value) == expected


@pytest.mark.asyncio
@pytest.mark.parametrize("event_acct_file", ["idem-high"], indirect=True)
async def test_high(hub, runpy, kafka, rabbitmq, event_acct_file):
    with tempfile.NamedTemporaryFile(mode="w", suffix=".sls") as fh:
        fh.write("s:\n  test.succeed_without_changes")
        fh.flush()

        proc = subprocess.Popen(
            [
                sys.executable,
                runpy,
                "state",
                fh.name,
                f"--acct-file={event_acct_file}",
                "--esm-plugin=null",
            ],
        )
        assert proc.wait() == 0

    path = pathlib.Path(fh.name)

    expected = {
        "message": {
            "s": {
                "__sls__": path.stem,
                "test": ["succeed_without_changes", {"order": 100000}],
            }
        },
        "tags": {"ref": "idem.resolve.introduce", "type": "state-high-data"},
        "run_name": "cli",
    }

    # Test rabbitmq
    received_message = await rabbitmq.get()
    assert json.loads(received_message.body) == expected

    # Test Kafka
    received_message = await kafka.getone()
    assert json.loads(received_message.value) == expected


@pytest.mark.asyncio
@pytest.mark.parametrize("event_acct_file", ["idem-status"], indirect=True)
async def test_status(hub, runpy, kafka, rabbitmq, event_acct_file):
    with tempfile.NamedTemporaryFile(mode="w", suffix=".sls") as fh:
        fh.write("s:\n  test.succeed_without_changes")
        fh.flush()

        proc = subprocess.Popen(
            [
                sys.executable,
                runpy,
                "state",
                fh.name,
                f"--acct-file={event_acct_file}",
                "--esm-plugin=null",
            ],
        )
        assert proc.wait() == 0

    await hub.pop.loop.sleep(2)

    for status in ("GATHERING", "COMPILING", "RUNNING", "FINISHED"):
        expected = {
            "message": status,
            "tags": {"ref": "idem.state.update_status", "type": "state-status"},
            "run_name": "cli",
        }

        # Test rabbitmq
        received_message = await rabbitmq.get()
        assert json.loads(received_message.body) == expected

        # Test Kafka
        received_message = await kafka.getone()
        assert json.loads(received_message.value) == expected


@pytest.mark.asyncio
@pytest.mark.parametrize("event_acct_file", ["idem-chunk"], indirect=True)
async def test_chunk(hub, runpy, kafka, rabbitmq, event_acct_file, kafka_ctx, pika_ctx):
    with tempfile.NamedTemporaryFile(mode="w", suffix=".sls") as fh:
        fh.write("s:\n  test.succeed_with_comment:\n    comment: asdf")
        fh.flush()

        proc = subprocess.Popen(
            [
                sys.executable,
                runpy,
                "state",
                fh.name,
                f"--acct-file={event_acct_file}",
                "--esm-plugin=null",
            ],
        )
        assert proc.wait() == 0

    path = pathlib.Path(fh.name)

    expected = {
        "message": {
            "__id__": "s",
            "__sls__": path.stem,
            "fun": "comment",
            "name": "s",
            "order": 1,
            "state": "test.succeed_with_comment",
        },
        "tags": {"ref": "idem.rules.init.run", "type": "state-chunk"},
        "run_name": "cli",
    }

    # Test rabbitmq
    received_message = await rabbitmq.get()
    assert json.loads(received_message.body) == expected

    # Test Kafka
    received_message = await kafka.getone()
    assert json.loads(received_message.value) == expected


@pytest.mark.asyncio
@pytest.mark.parametrize("event_acct_file", ["idem-state"], indirect=True)
async def test_state_pre_post(
    hub, runpy, kafka, rabbitmq, event_acct_file, kafka_ctx, pika_ctx
):
    with tempfile.NamedTemporaryFile(mode="w", suffix=".sls") as fh:
        fh.write("s:\n  test.succeed_without_changes")
        fh.flush()

        proc = subprocess.Popen(
            [
                sys.executable,
                runpy,
                "state",
                fh.name,
                f"--acct-file={event_acct_file}",
                "--esm-plugin=null",
            ],
        )
        assert proc.wait() == 0

    pathlib.Path(fh.name)

    expected_pre = {
        "message": {
            "s": {
                "test.succeed_without_changes": {
                    "ctx": {"old_state": None, "run_name": "cli", "test": False},
                    "kwargs": {},
                    "name": "s",
                }
            }
        },
        "run_name": "cli",
        "tags": {"ref": "states.test.succeed_without_changes", "type": "state-pre"},
    }

    # Test rabbitmq
    received_message = await rabbitmq.get()
    assert json.loads(received_message.body) == expected_pre

    # Test Kafka
    received_message = await kafka.getone()
    assert json.loads(received_message.value) == expected_pre

    expected_post = {
        "message": {
            "changes": {},
            "comment": "Success!",
            "name": "s",
            "result": True,
        },
        "tags": {"ref": "states.test.succeed_without_changes", "type": "state-post"},
        "run_name": "cli",
    }

    # Test rabbitmq
    received_message = await rabbitmq.get()
    assert json.loads(received_message.body) == expected_post

    # Test Kafka
    received_message = await kafka.getone()
    assert json.loads(received_message.value) == expected_post
