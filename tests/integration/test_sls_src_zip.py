import os
import pathlib
import tempfile
from zipfile import ZipFile

import pytest

from tests.runner import IdemRunException
from tests.runner import run_sls


def test_sls_src_zip(hub):

    tempdir = tempfile.mkdtemp()
    os.makedirs(tempdir, exist_ok=True)
    zipFile = os.path.join(tempdir, "sls_bundle.zip")
    createZip(
        pathlib.Path(__file__).parent.parent / "sls" / "zip_bundles" / "sls_bundle",
        zipFile,
    )
    ret = run_sls(
        ["entry"],
        sls_sources=[f"file://{zipFile}", f"file://{zipFile}"],
    )

    assert len(ret) == 6, f"Expecting 6 states {ret}"
    for state, ret in ret.items():
        assert ret["result"] is True, state


def test_sls_src_zip_missing(hub):

    tempdir = tempfile.mkdtemp()
    os.makedirs(tempdir, exist_ok=True)
    zipFile = os.path.join(tempdir, "sls_bundle.zip")
    createZip(
        pathlib.Path(__file__).parent.parent / "sls" / "zip_bundles" / "sls_bundle",
        zipFile,
    )
    with pytest.raises(IdemRunException) as e:
        run_sls(
            ["invalid_entry"],
            sls_sources=[f"file://{zipFile}", f"file://{zipFile}"],
        )
        assert "SLS ref 'invalid_entry' did not resolve from sources" == str(e)


def test_sls_src_zip_missing_include(hub):

    tempdir = tempfile.mkdtemp()
    os.makedirs(tempdir, exist_ok=True)
    zipFile = os.path.join(tempdir, "sls_bundle_missing_include.zip")
    createZip(
        pathlib.Path(__file__).parent.parent
        / "sls"
        / "zip_bundles"
        / "sls_bundle_missing_include",
        zipFile,
    )

    with pytest.raises(IdemRunException) as e:
        run_sls(
            ["entry"],
            sls_sources=[f"file://{zipFile}", f"file://{zipFile}"],
        )
        assert "SLS ref 'file_2' did not resolve from sources" == str(e)


def createZip(directory: str, zipFileLoc: str):
    with ZipFile(zipFileLoc, "w") as zip:
        for root, dirs, files in os.walk(directory):
            # add directory (needed for empty dirs)
            zip.write(root, os.path.relpath(root, directory))
            for file in files:
                filename = os.path.join(root, file)
                if os.path.isfile(filename):  # regular files only
                    arcname = os.path.join(os.path.relpath(root, directory), file)
                    zip.write(filename, arcname)
