from tests.runner import run_sls
from tests.runner import run_sls_validate


def assert_params(params):
    assert params
    assert params["rg_name"]
    assert isinstance(params["rg_name"], dict)
    assert params["rg_name"]["my_rg"] == ""
    assert params["rg_name"]["your_rg"] == "default"
    assert params["locations"]
    assert isinstance(params["locations"], list)
    assert len(params["locations"]) == 5
    assert isinstance(params["locations"][4]["state"], dict)
    assert params["locations"][4]["state"]["city"] == ""


def test_validate_params(hub):
    ret = run_sls_validate(["success"])
    assert ret
    params = ret["params"].params()
    assert_params(params)

    parameters = ret["parameters"]
    assert_params(parameters["GLOBAL"])
    assert parameters["ID_DECS"]
    parameters_state = parameters["ID_DECS"]
    assert (
        "success.Assure Resource Group Present {{ params.get('rg_name').get('my_rg') }}"
        in parameters_state
    )
    assert (
        "success.allowed-locations for {{ params.get('rg_name').get('my_rg') }}"
        in parameters_state
    )
    params = parameters_state[
        "success.allowed-locations for {{ params.get('rg_name').get('my_rg') }}"
    ]
    assert params["rg_name"]
    assert isinstance(params["rg_name"], dict)
    assert params["rg_name"]["my_rg"] == ""

    for key in ["ruleIds", "ruleDict", "ruleIdsString"]:
        state_id = f"success.{{{{ params.get('{key}') }}}}"
        assert state_id in parameters_state
        params = parameters_state[state_id]
        assert key in params
        assert isinstance(params[key], str)
        assert params[key] == ""

    warnings = ret["warnings"]
    assert warnings
    assert warnings["ID_DECS"]
    assert warnings["ID_DECS"][
        "success.Assure Resource Group Present {{ params.get('rg_name').get('my_rg') }}"
    ]
    id_warnings = warnings["ID_DECS"][
        "success.Assure Resource Group Present {{ params.get('rg_name').get('my_rg') }}"
    ]
    assert id_warnings["params_meta_missing"]
    assert "rg_name.your_rg" in id_warnings["params_meta_missing"]
    assert "locations" in id_warnings["params_meta_missing"]
    assert "locations[].state" in id_warnings["params_meta_missing"]

    for key in ["ruleIds", "ruleDict", "ruleIdsString"]:
        state_id = f"success.{{{{ params.get('{key}') }}}}"
        assert warnings["ID_DECS"][state_id]
        id_warnings = warnings["ID_DECS"][state_id]
        assert "params_meta_missing" in id_warnings
        assert key in id_warnings["params_meta_missing"]


def test_invalid_syntax(hub):
    # Verify a descriptive message is thrown with invalid syntax
    try:
        run_sls(["invalid_syntax"])
    except Exception as e:
        assert "Invalid syntax" in e.args[0]
