from tests.runner import run_sls


def test_trigger_build(hub, code_dir):
    ret = run_sls(["trigger"])
    assert len(ret) == 4, "Expecting 4 result"
    assert (
        ret[
            "trigger_|-trigger_state_with_changes_|-trigger_state_with_changes_|-build"
        ]["result"]
        is True
    )
    assert ret["test_|-should_run_state_|-should_run_state_|-nop"]["result"] is True
    assert (
        ret["trigger_|-trigger_state_no_changes_|-trigger_state_no_changes_|-build"][
            "result"
        ]
        is True
    )
    assert (
        ret["test_|-should_not_run_state_|-should_not_run_state_|-nop"]["result"]
        is False
    )

    assert not ret[
        "trigger_|-trigger_state_no_changes_|-trigger_state_no_changes_|-build"
    ]["changes"]
    assert ret[
        "trigger_|-trigger_state_with_changes_|-trigger_state_with_changes_|-build"
    ]["changes"]
