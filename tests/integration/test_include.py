import pytest
import rend.exc

from tests.runner import IdemRunException
from tests.runner import run_sls


def test_include(hub):
    ret = run_sls(["include"])
    assert ret
    assert len(ret) == 5, f"Expecting 5 states: {ret}"
    for state, ret in ret.items():
        assert ret["result"] is True, state


def test_include_missing_files(hub):
    with pytest.raises(IdemRunException) as e:
        run_sls(["invalid_include"])
        "SLS ref 'dir.non_existing_file' did not resolve from sources" == str(e)


def test_include_duplicates(hub):
    with pytest.raises(IdemRunException) as e:
        run_sls(["include_duplicates"])
        assert "Duplicate sls declarations found: duplicate_name" == str(e)


def test_duplicates(hub):
    with pytest.raises(rend.exc.RenderException) as e:
        run_sls(["duplicates"])
        assert (
            "RenderException: Yaml render error: found conflicting ID 'duplicate_name'"
            == str(e)
        )
