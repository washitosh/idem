test delay:
  test.succeed_without_changes:
    - require:
      - time.sleep: sleep_2s

sleep_2s:
  time.sleep:
    - duration: 2

test delay2:
  test.succeed_without_changes:
    - require:
      - time.sleep: invalid delay

invalid delay:
  time.sleep
