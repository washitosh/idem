trigger_state_with_changes:
    trigger.build:
        - triggers:
            - last_run: {{ range(1, 51) | random }}

should_run_state:
  test.nop:
    - onchanges:
      - trigger: trigger_state_with_changes

trigger_state_no_changes:
    trigger.build:
        - trigger:


should_not_run_state:
  test.nop:
    - onchanges:
      - trigger: trigger_state_no_changes
