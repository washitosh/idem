fails:
  test.fail_without_changes

fails_again:
  test.fail_without_changes

good:
  test.nop

runs:
  test.nop:
    - require_any:
      - test: fails
      - test: fails_again
      - test: good

bad:
  test.nop:
    - require_any:
      - test: fails
      - test: fails_again
