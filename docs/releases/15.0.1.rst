===========
Idem 15.0.1
===========

Idem 15.0.1 introduces '``old_state``' and '``new_state``' to state returns for the '``resource``' contract.
It is no longer required to return '``changes``' to implement '``resource``' contract  for Idem states.
Idem generates '``changes``' automatically based on '``old_state``' and '``new_state``' in state returns.

**Note**: It will be required to return '``old_state``' and '``new_state``' for '``resource``' contract in the future
release of Idem.
